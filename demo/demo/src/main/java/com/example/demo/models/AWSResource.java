package com.example.demo.models;

import org.springframework.stereotype.Component;

/**
 * Created by amehendale on 10/6/2017.
 */
@Component
public class AWSResource {
    private String id;
    private String resourceType;
    private String resourceId;
    private String logicalId;
    private String resourceCost;
    private String endDate;


    public AWSResource(String id, String resourceType, String resourceId, String logicalId, String resourceCost, String enddate) {
        this.id = id;
        this.resourceType = resourceType;
        this.resourceId = resourceId;
        this.logicalId = logicalId;
        this.resourceCost = resourceCost;
        endDate = enddate;
    }

    public AWSResource() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getLogicalId() {
        return logicalId;
    }

    public void setLogicalId(String logicalId) {
        this.logicalId = logicalId;
    }

    public String getResourceCost() {
        return resourceCost;
    }

    public void setResourceCost(String resourceCost) {
        this.resourceCost = resourceCost;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
