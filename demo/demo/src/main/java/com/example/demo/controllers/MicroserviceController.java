package com.example.demo.controllers;

import com.example.demo.models.AWSResource;
import com.example.demo.models.Microservice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amehendale on 10/3/2017.
 */
@RestController
@RequestMapping("/microservice")
public class MicroserviceController {

    @RequestMapping(path = "/getall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Microservice> getMicroservice() {
        //TODO: bind facade to fetch real data
        Microservice microservice = new Microservice("1", "Microservice-1", "30");
        Microservice microservice2 = new Microservice("2", "Microservice-2", "50");

        List<Microservice> msList = new ArrayList<>();
        msList.add(microservice);
        msList.add(microservice2);
        return msList;
    }

    public List<AWSResource> getMicroserviceResource(@RequestParam String msId) {
        System.out.println(msId);

        //TODO: bind facade to fetch real data
        AWSResource awsResource1 = new AWSResource("1", "AWS::S3::Bucket", "my-demo-stack-1-s3b33wet-fq1hjcweyvev", "S3B33WET", "17", "03-10-2017");
        AWSResource awsResource2 = new AWSResource("2", "AWS::S3::Bucket", "my-demo-stack-1-s3b3cik7-q2kosrl29uoi", "S3B3CIK7", "13", "03-10-2017");

        List<AWSResource> resourceList = new ArrayList<>();
        resourceList.add(awsResource1);
        resourceList.add(awsResource2);

        return resourceList;


    }

}
