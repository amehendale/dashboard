package com.example.demo.models;

/**
 * Created by amehendale on 10/3/2017.
 */
public class Microservice {
    private String id;
    private String name;
    private String totalCost;

    public Microservice() {
    }

    public Microservice(String id, String name, String totalCost) {
        this.id = id;
        this.name = name;
        this.totalCost = totalCost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }
}
